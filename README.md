# instagram-app

## Introducción

Esta aplicación está implementada utilizando el framework ReactJS. Consiste de una SPA (Single Page Application), la cual permite crear contenidos, visualizar los contenidos creados y ver el feed de los usuarios.

## Ejecución

En primer lugar, dentro del root del directorio del proyecto, se deberá ejecutar el comando `npm install` para instalar todas las dependencias de la aplicación.

Luego, se deberá ejecutar el comando `npm start`, el cual inicializará un servidor web sirviendo la aplicación en el puerto 3000, siendo posible accederla a través de la URL: http://localhost:3000.

Para poder loggearse, utilizar las siguientes credenciales del usuario de **test**:

**Email:** test@test.com, 
**Contraseña:** test

## Configuración

La única configuración que se debe realizar es la dirección URL de la API Rest para la interacción del frontend con el backend. Esta URL se encuentra configurada dentro del directorio *src/*, en el archivo constants.js. En el caso de querer modificar la URL, modificar la constante llamada **BASE_URL**. 