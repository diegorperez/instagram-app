// react
import React from 'react';
import ReactDOM from 'react-dom';

// styles
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/react-progress-2/main.css'
import '../node_modules/font-awesome/css/font-awesome.min.css';

// components
import App from './App';

import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App /> , document.getElementById('app'));
registerServiceWorker();