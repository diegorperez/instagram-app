// react
import React, { Component } from 'react';

// constants
import { apiEndpoints, testUser } from '../constants.js';

import ImageFilter from 'react-image-filter';
import Navbar from '../Navbar/Navbar.jsx'

// styles
import './Feed.sass';

// axios
import axios from 'axios';

class Feed extends Component {

    constructor(props) {
        super(props);

        this.state = {
            contents: []
        };
    }

    componentDidMount() {
        axios.get(apiEndpoints.FEED + "?id=" + testUser.ID)
        .then((response) => {
            this.setState({
                contents: response.data
            })
        })
        .catch((error) => {
            console.log(error);
        });
    }

    getHashtagList(hashtags) {
        let text = "";
        for (var i = 0; i < hashtags.length; i++) { 
            text += "#" + hashtags[i] + " ";
        }
        return text
    }

    render() {

        return (
            <div>
                <Navbar/>
                <div className='title'>
                    <h2>Feed</h2>
                </div>
                { this.state.contents.length > 0 && this.state.contents.map(content => 
                    <div className='feed' key={content.content_urls[0]}>
                        <div>
                            <div className='profile'>
                                <img src={content.user.profile_image_url} alt=''/>
                                <span>{content.user.nickname}</span>
                                <i className='fa fa-ellipsis-h fa-sm'></i>
                            </div>
                            <div className='image'>
                                { content.filter !== 'none' 
                                    ? <ImageFilter
                                        image={content.content_urls[0]}
                                        filter={content.filter} />
                                    : <img src={content.content_urls[0]} alt=''/>
                                }
                            </div>
                            <div className='options'>
                                <i className='fa fa-heart fa-lg'></i>
                                <i className='fa fa-comment fa-lg'></i>
                            </div>
                            <div className='likes'>
                                {content.likes} Me gusta
                            </div>
                            <div className='nickname'>
                                <p><b>{content.user.nickname} </b> 
                                    <span className='description'> {content.description}</span> 
                                    <span className='hashtags'> {this.getHashtagList(content.hashtags)}</span>
                                </p> 
                            </div>
                            { content.comments > 0 && 
                                <div className='comments'>
                                    Ver los {content.comments} comentarios
                                </div>
                            }
                        </div>
                    </div>
                )}
                { this.state.contents.length === 0 &&
                    <div className='alert-content'>
                        <div className='alert alert-warning alert-dismissible fade show' role='alert'>
                            <strong>No hay contenido disponible para mostrar.</strong>
                            <button type='button' className='close' data-dismiss='alert' aria-label='Close'>
                                <span aria-hidden='true'>&times;</span>
                            </button>
                        </div>
                    </div> 
                }
            </div>
        );
    }
}
  
export default Feed;
  