const BASE_URL = "http://localhost:7070/";

export const appRoutes = {
    HOME: '/',
    LOGIN: '/login',
    CREATE_CONTENT: '/contents/create',
    VISUALIZE_CONTENT: '/contents',
    FEED: '/feed'
};

export const apiEndpoints = {
    CREATE_CONTENT: BASE_URL + 'contents/create',
    VISUALIZE_CONTENT: BASE_URL + 'contents/retrieve',
    FEED: BASE_URL + 'feed'
};

export const testUser = {
    EMAIL: 'test@test.com', 
    NICKNAME: 'test',
    PASSWORD: 'test',
    ID: 'ef152e9b-7442-4bfc-9d4d-dee68b4063d2'
};