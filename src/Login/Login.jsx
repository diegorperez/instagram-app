// react
import React, { Component } from 'react';

// constants
import { appRoutes, testUser } from '../constants.js';

import Progress from "react-progress-2";

// styles
import './Login.sass';

class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            error: false
        };

    }

    handleInputChange(event) {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({
            [name]: value
        });
    }

    handleKeyPress(e){
        if (e.key === 'Enter') {
            this.tryLogin();
        }
    }

    tryLogin() {
        var email = this.state.email;
        var password = this.state.password;

        Progress.show();
        setTimeout(
            function() {
                Progress.hide();
                if (email === testUser.EMAIL && password === testUser.PASSWORD) {
                    this.redirectToFeed();
                } else {
                    this.setLoginError();
                }              
            }
            .bind(this),
            2000
        );
    }

    setLoginError = () => {
        this.setState({
            error: true
        });
    }

    redirectToFeed() {
        window.location.assign(appRoutes.FEED);
    }

    render() {
        return (
            <div>
                <Progress.Component/>
                <div className='login'>
                    <div>
                        <img className="logo" src='instagram_logo.png' alt=''></img>
                        <form>
                            <div className='form-group'>
                                <input name='email' type='email' className='form-control' placeholder='Teléfono, usuario o correo electrónico' onChange={this.handleInputChange.bind(this)} />
                            </div>

                            <div className='form-group'>
                                <input name='password' type='password' className='form-control' placeholder='Contraseña' onChange={this.handleInputChange.bind(this)} />
                            </div>

                            { this.state.error &&
                                <div className='form-group login-error'>
                                    Error al intentar iniciar sesión. Verifique el email y la contraseña.
                                </div>
                            }

                            <button type='button' className='btn btn-primary full-width' onClick={this.tryLogin.bind(this)}>Iniciar sesión</button>
                        </form>
                        <a className="forgot-password" href=''>¿Olvidaste tu contraseña?</a>
                    </div>  
                    <div className="download-app">
                        <p>Descargar aplicación</p>   
                        <div>
                            <a href="https://itunes.apple.com/app/instagram/id389801252?pt=428156&ct=igweb.loginPage.badge&mt=8"><img src='app-store.png' alt=''></img></a>
                            <a href="https://play.google.com/store/apps/details?id=com.instagram.android&referrer=utm_source%3Dinstagramweb%26utm_campaign%3DloginPage%26utm_medium%3Dbadge"><img src='google-play.png' alt=''></img></a>
                        </div> 
                        
                    </div> 
                </div> 
            </div>  
        );
    }
}
  
export default Login;
  