// react
import React, { Component } from 'react';

// constants
import { appRoutes, apiEndpoints, testUser } from '../constants.js';

import Progress from 'react-progress-2';
import { WithContext as ReactTags } from 'react-tag-input';
import ImageFilter from 'react-image-filter';
import Navbar from '../Navbar/Navbar.jsx'

// styles
import './Content.sass';

// axios
import axios from 'axios';

// content types
const TYPE_IMAGE = 'image';

// content load types
const LOAD_TYPE_SINGLE = 'single';

// share types
const SHARE_IN_FACEBOOK = 'facebook';
const SHARE_IN_TWITTER = 'twitter';

// filter types
const SEPIA = 'sepia';
const GRAYSCALE = 'grayscale';
const INVERT = 'invert';
const NONE = 'none';

const KeyCodes = { enter: 13 };
const delimiters = [KeyCodes.enter];
const suggestions = [];

class CreateContent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            type: TYPE_IMAGE,
            load_type: LOAD_TYPE_SINGLE,
            facebook_share: '',
            twitter_share: '',
            filter: '',
            hashtags: [],
            tagged_users: [],
            contentPreviewUrl: '',
            content_name: '',
            content_width: 0,
            content_height: 0
        };

        this.onShareChanged = this.onShareChanged.bind(this);
        this.onFilterChange = this.onFilterChange.bind(this);
        this.handleHashtagDelete = this.handleHashtagDelete.bind(this);
        this.handleHashtagAddition = this.handleHashtagAddition.bind(this);
        this.handleHashtagDrag = this.handleHashtagDrag.bind(this);
        this.handleTagUserDelete = this.handleTagUserDelete.bind(this);
        this.handleTagUserAddition = this.handleTagUserAddition.bind(this);
        this.handleTagUserDrag = this.handleTagUserDrag.bind(this);
        this.handleImageChange = this.handleImageChange.bind(this);
    }

    handleInputChange(event) {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({
            [name]: value
        });
    }

    handleHashtagDelete(i) {
        const { hashtags } = this.state;
        this.setState({
            hashtags: hashtags.filter((tag, index) => index !== i),
        });
    }
 
    handleHashtagAddition(tag) {
        this.setState(state => ({ hashtags: [...state.hashtags, tag] }));
    }

    handleHashtagDrag(tag, currPos, newPos) {
        const hashtags = [...this.state.hashtags];
        const newTags = hashtags.slice();
 
        newTags.splice(currPos, 1);
        newTags.splice(newPos, 0, tag);
 
        this.setState({ hashtags: newTags });
    }
 
    handleTagUserDrag(tag, currPos, newPos) {
        const tagged_users = [...this.state.tagged_users];
        const newTags = tagged_users.slice();
 
        newTags.splice(currPos, 1);
        newTags.splice(newPos, 0, tag);
 
        this.setState({ tagged_users: newTags });
    }

    handleTagUserDelete(i) {
        const { tagged_users } = this.state;
        this.setState({
            tagged_users: tagged_users.filter((tag, index) => index !== i),
        });
    }
 
    handleTagUserAddition(tag) {
        this.setState(state => ({ tagged_users: [...state.tagged_users, tag] }));
    }

    handleKeyPress(e){
        if (e.key === 'Enter') {
            this.tryCreateContent();
        }
    }

    onFilterChange(e) {
        this.setState({
            filter: e.currentTarget.value
        });
    }

    onShareChanged(e) {
        if (e.currentTarget.name === "share_facebook") {
            this.setState({
                facebook_share: e.currentTarget.value
            });
        } else {
            this.setState({
                twitter_share: e.currentTarget.value
            });
        }  
    }

    tryCreateContent() {
        Progress.show();
        let content = this.buildContentRequest();
        axios.post(apiEndpoints.CREATE_CONTENT, content)
        .then(function (response) {
            Progress.hide();
            if (response.status === 200) {
                window.location.assign(appRoutes.VISUALIZE_CONTENT + '/' + response.data.id);
            }
        })
        .catch(function (error) {
            console.log(error);
            Progress.hide();
        });
    }

    getTags() {
        let len = this.state.hashtags.length;
        let tags = [];
        for (let i = 0; i < len; i++) {
            tags.push(this.state.hashtags[i].text)
        }
        
        return tags;
    }

    getTaggedUsers() {
        let len = this.state.tagged_users.length;
        let tagged_users = [];
        for (let i = 0; i < len; i++) {
            tagged_users.push(this.state.tagged_users[i].text)
        }
        
        return tagged_users;
    }

    getShares() {
        let share_in = [];
        if (this.state.facebook_share) {
            share_in.push(SHARE_IN_FACEBOOK);
        }
        if (this.state.twitter_share) {
            share_in.push(SHARE_IN_TWITTER);
        }
        return share_in;
    }

    buildContentRequest() {
        let tags = this.getTags();
        let tagged_users = this.getTaggedUsers();
        let share_in = this.getShares();
        let filter = this.state.filter;
        if (filter === '') {
            filter = NONE;
        }

        let content = {
            type: this.state.type,
            load_type: this.state.load_type,
            user_id: testUser.ID,
            tags: tags,
            filter: filter,
            description: this.state.description,
            tagged_users_nicknames: tagged_users,
            share_in: share_in,
            contents: [{
                data: this.state.contentPreviewUrl,
                name: this.state.content_name,
                width: this.state.content_width,
                heigth: this.state.content_height
            }]
        }

        return content;
    }

    handleImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];
        var image = new Image();

        reader.onloadend = () => {
            this.setState({
                content_name: file.name,
                contentPreviewUrl: reader.result
            });
            image.src = this.state.contentPreviewUrl
            image.onload = () => {
                this.setState({
                    content_width: image.width,
                    content_height: image.height
                });
            };
        }

        reader.readAsDataURL(file)
    }

    getImagePreview() {
        if (this.state.filter) {
            return <ImageFilter
                        className='content-preview'
                        image={this.state.contentPreviewUrl}
                        filter={this.state.filter} />
        } else {
            return <img className='content-preview' src={this.state.contentPreviewUrl} alt=''></img>
        } 
    }

    render() {
        const { hashtags, tagged_users, contentPreviewUrl, filter } = this.state;

        return (
            <div>
                <Navbar/>
                <Progress.Component/>
                <div className='content'>
                    <div>
                        <h1>Crear nuevo contenido</h1>
                        <form>
                            <fieldset className="form-group">
                                <div className="row">
                                    <legend className="col-form-label col-sm-4 pt-0">Descripción</legend>
                                    <div className="col-sm-8">
                                    <input name='description' type='description' className='form-control' placeholder='Descripción' onChange={this.handleInputChange.bind(this)} />
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset className="form-group">
                                <div className="row">
                                    <legend className="col-form-label col-sm-4 pt-0">Hashtags</legend>
                                    <div className="col-sm-8">
                                        <ReactTags tags={hashtags}
                                            suggestions={suggestions}
                                            handleDelete={this.handleHashtagDelete}
                                            handleAddition={this.handleHashtagAddition}
                                            handleDrag={this.handleHashtagDrag}
                                            delimiters={delimiters}
                                            placeholder='Agregar nuevo hashtag' />
                                    </div>
                                </div>
                            </fieldset>

                             <fieldset className="form-group">
                                <div className="row">
                                    <legend className="col-form-label col-sm-4 pt-0">Etiquetas</legend>
                                    <div className="col-sm-8">
                                        <ReactTags tags={tagged_users}
                                            suggestions={suggestions}
                                            handleDelete={this.handleTagUserDelete}
                                            handleAddition={this.handleTagUserAddition}
                                            handleDrag={this.handleTagUserDrag}
                                            delimiters={delimiters}
                                            placeholder='Etiquetar usuario' />
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset className="form-group">
                                <div className="row">
                                    <legend className="col-form-label col-sm-4 pt-0">Compartir en</legend>
                                    <div className="col-sm-8">
                                        <div className="form-check">
                                            <input className="form-check-input" type="checkbox" name="share_facebook" id="share_facebook" 
                                                    value={SHARE_IN_FACEBOOK} 
                                                    checked={this.state.facebook_share === SHARE_IN_FACEBOOK} 
                                                    onChange={this.onShareChanged}/>
                                            <label className="form-check-label">
                                                Facebook
                                            </label>
                                        </div>
                                        <div className="form-check">
                                            <input className="form-check-input" type="checkbox" name="share_twitter" id="share_twitter"
                                                    value={SHARE_IN_TWITTER} 
                                                    checked={this.state.twitter_share === SHARE_IN_TWITTER} 
                                                    onChange={this.onShareChanged}/>
                                            <label className="form-check-label">
                                                Twitter
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset className="form-group">
                                <div className="row">
                                    <legend className="col-form-label col-sm-4 pt-0">Contenido</legend>
                                    <div className="col-sm-8">
                                        <input type="file" onChange={this.handleImageChange}/>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset className="form-group">
                                <div className="row">
                                    <legend className="col-form-label col-sm-4 pt-0">Vista previa</legend>
                                    <div className="col-sm-8">
                                        {contentPreviewUrl ? 
                                        (
                                            this.getImagePreview()
                                        ) : (
                                            <p>No hay contenido para mostrar</p>
                                        )}
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset className="form-group">
                                <div className="row">
                                    <legend className="col-form-label col-sm-4 pt-0">Filtro</legend>
                                    <div className="col-sm-8">
                                        <div className="form-check">
                                            <input className="form-check-input" type="radio" name="sepia" id="sepia" 
                                                    value={SEPIA} 
                                                    checked={filter === SEPIA} 
                                                    onChange={this.onFilterChange}/>
                                            <label className="form-check-label">
                                                Sepia
                                            </label>
                                        </div>
                                        <div className="form-check">
                                            <input className="form-check-input" type="radio" name="invert" id="invert"
                                                    value={INVERT} 
                                                    checked={filter === INVERT} 
                                                    onChange={this.onFilterChange}/>
                                            <label className="form-check-label">
                                                Invertido
                                            </label>
                                        </div>
                                        <div className="form-check">
                                            <input className="form-check-input" type="radio" name="grayscale" id="grayscale"
                                                    value={GRAYSCALE} 
                                                    checked={filter === GRAYSCALE} 
                                                    onChange={this.onFilterChange}/>
                                            <label className="form-check-label">
                                                Escala de grises
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <button type='button' className='btn btn-primary full-width create-content' onClick={this.tryCreateContent.bind(this)}>Crear contenido</button>
                        </form>
                    </div>  
                </div> 
            </div>  
        );
    }
}
  
export default CreateContent;
  