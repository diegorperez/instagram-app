// react
import React, { Component } from 'react';

// constants
import { apiEndpoints } from '../constants.js';

import ImageFilter from 'react-image-filter';
import Navbar from '../Navbar/Navbar.jsx'

// styles
import './Content.sass';

// axios
import axios from 'axios';

class VisualizeContent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            content: null
        };
    }

    componentDidMount() {
        let contentId = this.props.match.params.id;
        axios.get(apiEndpoints.VISUALIZE_CONTENT + '?id=' + contentId)
        .then((response) => {
            if (response.status === 200) {
                this.setState({
                    content: response.data
                })
                console.log(response.data);
            }
        })
        .catch((error) => {
            console.log(error);
        });
    }

    getHashtagList(hashtags) {
        let text = "";
        for (var i = 0; i < hashtags.length; i++) { 
            text += "#" + hashtags[i] + " ";
        }
        return text
    }

    render() {
        let content = this.state.content;

        return (
            <div>
                <Navbar/>
                <div className='title'>
                    <h2>Contenido</h2>
                </div>
                { content !== null &&
                    <div className='content' key={content.id}>
                        <div>
                            <div className='profile'>
                                <img src={content.user.profile_image_url} alt=''/>
                                <span>{content.user.nickname}</span>
                                <i className='fa fa-ellipsis-h fa-sm'></i>
                            </div>
                            <div className='image'>
                                { content.filter !== 'none' 
                                    ? <ImageFilter
                                        image={content.contents[0].urls.content_url}
                                        filter={content.filter} />
                                    : <img src={content.contents[0].urls.content_url} alt=''/>
                                }
                            </div>
                            <div className='options'>
                                <i className='fa fa-heart fa-lg'></i>
                                <i className='fa fa-comment fa-lg'></i>
                            </div>
                            <div className='likes'>
                                {content.likes} Me gusta
                            </div>
                            <div className='nickname'>
                                <p><b>{content.user.nickname} </b> 
                                    <span className='description'> {content.description}</span> 
                                    <span className='hashtags'> {this.getHashtagList(content.tags)}</span>
                                </p> 
                            </div>
                            { content.comments > 0 && 
                                <div className='comments'>
                                    Ver los {content.comments} comentarios
                                </div>
                            }
                        </div>
                    </div>
                }
            </div>
        );
    }
}
  
export default VisualizeContent;
  