// react
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'

// constants
import { appRoutes } from './constants.js'

// styles
import './App.sass';

// components
import Login from './Login/Login.jsx'
import CreateContent from './Content/CreateContent.jsx'
import VisualizeContent from './Content/VisualizeContent.jsx'
import Feed from './Feed/Feed.jsx'

class App extends Component {

  render() {
    return (
      <Router>
          <Switch>
            <Route exact path="/" render={() => (<Redirect to={appRoutes.LOGIN} />)}/>
            <Route path={appRoutes.LOGIN} component={Login}/>
            <Route path={appRoutes.CREATE_CONTENT} component={CreateContent}/>
            <Route path={appRoutes.VISUALIZE_CONTENT + '/:id'} component={VisualizeContent}/>
            <Route path={appRoutes.FEED} component={Feed}/>
          </Switch>
      </Router>
    );
  }

}

export default App;