// react
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'

// constants
import { appRoutes } from '../constants.js';

// styles
import './Navbar.sass';

class Navbar extends Component {

    constructor(props) {
        super(props);

        this.state = {
            redirect: false
        };

        this.logout = this.logout.bind(this);
    }

    setRedirect = () => {
        this.setState({
            redirect: true
        });
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to={appRoutes.HOME}/>
        }
    }

    logout() {
        this.setRedirect();
    }

    render() {
        return (
            <div className='top-navbar'>
                {this.renderRedirect()}
                <nav className='navbar navbar-light'>
                    <ul className='nav navbar-top-links navbar-left'>
                        <a className='navbar-brand' href={appRoutes.HOME}>Instagram</a>
                        <a className='nav-link' href={appRoutes.FEED}>Feed</a>
                        <a className='nav-link' href={appRoutes.CREATE_CONTENT}>Crear contenido</a>
                    </ul>

                    <ul className='nav navbar-top-links navbar-right'>
                        <li className='logout-button'>
                            <a className='top-nav-button' onClick={this.logout}>
                                <i className='fa fa-sign-out'></i>
                                <span>Log Out</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        );
    }

}

export default Navbar;